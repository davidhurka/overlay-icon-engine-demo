# OverlayIconEngine demo project

OverlayIconEngine is a QIconEngine which allows to place colorful overlays on any QIcon.
This project contains OverlayIconEngine and a simple Qt Widgets application to demonstrate its abilities.
Below is a screenshot of a monochrome Breeze icon with two overlays.

![Screenshot of overlay-icon-engine-demo with color-mode-black-white icon](doc/Screenshot_Color_Drops.png)

## OverlayIconEngine description

OverlayIconEngine adds overlays to QIcons.
Both the base icon and the overlays are supplied as QIcon,
so the icons can be fetched from the system icon theme,
and e. g. KIconLoader’s (Breeze style) functionality is preserved.
Specifically, Breeze icons are still recolored with the system color scheme,
which wouldn’t be the case if one simply paints an overlay on a pixmap and passes that to a QIcon.

Of course it is also possible to load the icons from Qt’s resource system.

QIcons with overlay are created by constructing one OverlayIconEngine with the base icon,
adding overlays with addOverlay, and constructing a QIcon from that engine.

If the icon shall be fetched from the system’s icon theme,
there is a convenience function OverlayIconEngine::fromTheme(),
to which you simply pass the base icon name and the overlay icon names and colors.
OverlayIconEngine::fromTheme() directly returns a QIcon.

### hiDPI

This is a bit hacky. OverlayIconEngine fetches pixmaps from the base and overlay icons.
At hiDPI, QIcon is expected to return bigger pixmaps than requested,
and then the pixmaps will have a device pixel ratio larger than 1.
But OverlayIconEngine needs pixmaps of exactly the requested size,
so it has to request smaller pixmaps than needed.
To calculate the smaller size, it fetches one pixmap from the icon,
discards it, and uses that pixmap’s device pixel ratio to calculate the appropriate smaller size.

The QIcon constructed from OverlayIconEngine will return pixmaps with the correct device pixel ratio.

## overlay-icon-engine-demo description

overlay-icon-engine-demo constructs a QIcon through OverlayIconEngine
and displays it in various sizes using QLabels and QToolButtons,
so focus and hover effects can be tested.

There are four QToolButtons, the third one can be checked,
and the fourth one has a menu trough which the third one can be
disabled, or an action in the third one’s menu can be disabled.
The QToolButtons have sizes 16px, 22px, 32px, and the odd size 32x25px.

There are three QLabels, which show a zoomed still image of the icon.
They have sizes 16px, 22px, and 22px, where the last size is in actual pixels, not device independent pixels.

## overlay-icon-engine-demo usage

overlay-icon-engine-demo is called with the name of the base icon,
and any number of names and colors for the overlay icons.
With the `--from-file` option, the names represent file paths.
Without the `--from-file` option, the icons are fetched from the system icon theme.
In the latter case, KIconLoader will be used and all sizes of the icon are loaded (if you use the Breeze style).

```shell
$ overlay-icon-engine-demo --from-file base-icon.svg overlay-icon.svg blue
$ overlay-icon-engine-demo base-icon overlay-icon blue overlay-icon2 lime
```

In the console output you can see which sizes are requested from the OverlayIconEngine,
and which sizes are fetched by the OverlayIconEngine.
If these sizes differ, something is wrong. (See section hiDPI.)
