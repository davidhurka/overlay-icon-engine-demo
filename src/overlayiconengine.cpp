/***************************************************************************
 *   Copyright (C) 2019-2020 by David Hurka <david.hurka@mailbox.org>      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 ***************************************************************************/

#include "overlayiconengine.h"

#include <QPainter>

OverlayIconEngine::OverlayIconEngine(const QIcon &baseIcon)
    : m_baseIcon(baseIcon)
{
}

void OverlayIconEngine::addOverlay(const QIcon &overlayIcon, const QColor &overlayColor)
{
    if (!overlayIcon.isNull() && overlayColor.isValid() && (overlayColor != Qt::transparent)) {
        m_overlayIcons.append(overlayIcon);
        m_overlayColors.append(overlayColor);
    } else {
        m_rectangleColors.append(overlayColor);
    }
}

QPixmap OverlayIconEngine::pixmap(const QSize &size, QIcon::Mode mode, QIcon::State state)
{
    QPixmap ret_pixmap(size);
    ret_pixmap.fill(Qt::transparent);

    QPainter painter(&ret_pixmap);
    paint(&painter, QRect(QPoint(0, 0), size), mode, state);

    return ret_pixmap;
}

void OverlayIconEngine::paint(QPainter *painter, const QRect &rect, QIcon::Mode mode, QIcon::State state)
{
    painter->save();
    painter->setPen(Qt::NoPen);

    // Paint base icon
    m_baseIcon.paint(painter, rect, Qt::AlignCenter, mode, state);

    // Paint recolored overlay icons on top of the base icon.
    for (int i_layer = 0; i_layer < m_overlayIcons.count(); ++i_layer) {
        QPixmap overlayPixmap = getOverlayPixmap(m_overlayIcons.at(i_layer), m_overlayColors.at(i_layer), rect.size());
        painter->drawPixmap(rect.topLeft(), overlayPixmap);
    }

    // Invalid overlay icons: Draw rectangles at bottom edge.
    for (int i_rectangle = 0; i_rectangle < m_rectangleColors.count(); ++i_rectangle) {
        if (!m_rectangleColors.at(i_rectangle).isValid() || (m_rectangleColors.at(i_rectangle) == Qt::transparent)) {
            continue;
        }

        painter->setBrush(m_rectangleColors.at(i_rectangle));
        const QPoint a(i_rectangle * rect.width() / m_rectangleColors.count(), rect.height() * 3 / 4);
        const QPoint b((i_rectangle + 1) * rect.width() / m_rectangleColors.count(), rect.height());
        painter->drawRect(QRect(a, b).adjusted(0, 0, -1, -1)); // Shrink because of QRect outline definition.
    }

    painter->restore();
}

QIconEngine *OverlayIconEngine::clone() const
{
    OverlayIconEngine *newEngine = new OverlayIconEngine(m_baseIcon);

    for (int i_layer = 0; i_layer < m_overlayIcons.count(); ++i_layer) {
        newEngine->addOverlay(m_overlayIcons.at(i_layer), m_overlayColors.at(i_layer));
    }
    for (int i_rectangle = 0; i_rectangle < m_rectangleColors.count(); ++ i_rectangle) {
        newEngine->addOverlay(QIcon(), m_rectangleColors.at(i_rectangle));
    }

    return newEngine;
}

QIcon OverlayIconEngine::fromTheme(const QString& baseIconName,
                                   const QList<QPair<QString, QColor>> overlays)
{
    OverlayIconEngine *engine = new OverlayIconEngine(QIcon::fromTheme(baseIconName));
    for (const QPair<const QString&, const QColor&> overlay : qAsConst(overlays)) {
        engine->addOverlay(QIcon::fromTheme(overlay.first), overlay.second);
    }
    return QIcon(engine);
}

QPixmap OverlayIconEngine::getOverlayPixmap(const QIcon &icon, const QColor &overlayColor, QSize size)
{
    const QRect pixmapRect(QPoint(0, 0), size);
    QPixmap ret_pixmap(size);
    ret_pixmap.fill(Qt::transparent);

    // Get overlay shape
    QPainter painter(&ret_pixmap);
    icon.paint(&painter, pixmapRect);

    // Fill shape with overlay color
    painter.setCompositionMode(QPainter::CompositionMode_SourceIn);
    painter.setPen(Qt::NoPen);
    painter.setBrush(overlayColor);
    painter.drawRect(pixmapRect);

    painter.end();

    return ret_pixmap;
}
