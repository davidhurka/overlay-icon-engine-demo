/***************************************************************************
 *   Copyright (C) 2019-2020 by David Hurka <david.hurka@mailbox.org>      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 ***************************************************************************/

#pragma once

#include <QIconEngine>

/**
 * Icon engine to generate QIcons with colorful overlays.
 *
 * Icon pixmaps are constructed by fetching a pixmap from the base icon,
 * and painting recolored pixmaps of overlay icons on top of that.
 *
 * If the base icon is provided trough QIcon::fromTheme(),
 * it will be recolored trough Breeze’s QIconEngine (if that theme is used).
 *
 * The base icon can be a regular Breeze icon.
 * The overlay icons can be any icon, only the shape is respected.
 * Therefore, overlay icons should have opaque fills.
 *
 * In the intended use case, the breeze icon theme provides the base icons and all overlays.
 * If the overlays are missing, OverlayIconEngine will instead use colored rectangles
 * at the bottom edge of the icon.
 * If the base icon is missing, only the overlays are shown.
 *
 * The overlay icons must provide the same maximum sizes as the base icon.
 *
 * To use this class, construct an instance with the base icon.
 * Then add overlays as needed with addOverlay().
 * Then create a QIcon from this engine.
 * Note that QIcon takes ownership of this engine.
 * OverlayIconEngine::fromTheme() can be used as convenience function.
 *
 * To change overlay colors, create a new OverlayIconEngine and a new QIcon.
 *
 * addPixmap() functions will not work.
 */
class OverlayIconEngine : public QIconEngine
{
public:
    /**
     * Constructs an overlay icon engine based on @p baseIcon.
     */
    explicit OverlayIconEngine(const QIcon &baseIcon);

    OverlayIconEngine(const OverlayIconEngine &) = delete;
    OverlayIconEngine &operator=(const OverlayIconEngine &) = delete;

    /**
     * Adds an overlay.
     *
     * Each overlay is painted above the base icon and all previous overlays.
     *
     * @param overlayIconName The overlay icon. Only shape is respected, so fills should be opaque.
     * @param overlayColor The color in which the overlay will be colored.
     */
    void addOverlay(const QIcon &overlayIcon, const QColor &overlayColor);

    /**
     * Paints base icon and overlays on @p painter.
     *
     * Called by pixmap().
     */
    void paint(QPainter *painter, const QRect &rect, QIcon::Mode mode, QIcon::State state) override;

    /**
     * Called by QIcon to get a pixmap.
     *
     * The returned pixmap will actually be of size @p size,
     * and have a device pixel ratio of 1.0.
     */
    QPixmap pixmap(const QSize &size, QIcon::Mode mode, QIcon::State state) override;

    /**
     * Creates an independent copy of this icon engine.
     */
    QIconEngine *clone() const override;

    /**
     * Convenience function.
     * Returns an icon from the icon theme, with the recolored overlays applied.
     *
     * @param baseIconName Name of the base icon, to be used with QIcon::fromTheme().
     * @param overlays Icon names and colors of overlays, names may be QString().
     */
    static QIcon fromTheme(const QString &baseIconName,
                           const QList<QPair<QString, QColor>> overlays);

protected:
    /**
     * Creates a pixmap of size @p size from @p icon and colorizes it in @p overlayColor.
     */
    QPixmap getOverlayPixmap(const QIcon &icon, const QColor &overlayColor, QSize size);

    QIcon m_baseIcon;
    QVector<QIcon> m_overlayIcons;
    QVector<QColor> m_overlayColors;
    /** Used for overlays of invalid icons */
    QVector<QColor> m_rectangleColors;
};
