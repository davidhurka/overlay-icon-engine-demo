/***************************************************************************
 *   Copyright (C) 2019-2020 by David Hurka <david.hurka@mailbox.org>      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 ***************************************************************************/

#include <QAction>
#include <QApplication>
#include <QCommandLineParser>
#include <QDebug>
#include <QLabel>
#include <QMenu>
#include <QToolButton>
#include <QVBoxLayout>
#include <QWidget>

#include "overlayiconengine.h"

QIcon createIcon(const QCommandLineParser &parser);
void createIconDemoWidget(QWidget &demoWidget, const QIcon &icon);

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QCommandLineParser parser;
    parser.setApplicationDescription(QApplication::translate("main",
        "Application to test OverlayIconEngine.\n"
        "Arguments: [options] <base icon> [<overlay icon> <color> ...]\n"
        "Use - to specify an invalid overlay icon."));
    parser.addHelpOption();

    QCommandLineOption fromFileOption(QStringList() << "f" << "from-file",
                                      QApplication::translate("main",
                                          "Fetch icons from QIcon(<name>), "
                                          "otherwise QIcon::fromTheme(<name>) is used."));
    parser.addOption(fromFileOption);

    QCommandLineOption noHidpiOption(QStringList() << "n" << "no-hidpi",
                                     QApplication::translate("main",
                                         "Disable Qt hiDPI mechanisms"));
    parser.addOption(noHidpiOption);

    QCommandLineOption convenienceFunctionOption(QStringList() << "c" << "convenience-function",
                                                 QApplication::translate("main",
                                                     "Test OverlayIconEngine::fromTheme() "
                                                     "instead of addOverlay()."));
    parser.addOption(convenienceFunctionOption);

    parser.process(app);

    if (parser.isSet(noHidpiOption)) {
        QCoreApplication::setAttribute(Qt::AA_UseHighDpiPixmaps, false);
    } else {
        QCoreApplication::setAttribute(Qt::AA_UseHighDpiPixmaps, true);
    }

    QIcon icon = createIcon(parser);

    QWidget demoWidget;
    createIconDemoWidget(demoWidget, icon);

    demoWidget.show();

    return app.exec();
}

QIcon createIcon(const QCommandLineParser &parser)
{
    QStringList iconNames = parser.positionalArguments();
    if (iconNames.isEmpty()) {
        qCritical() << "No icon names given";
        exit(1);
    }

    QIcon baseIcon;
    if (parser.isSet(QStringLiteral("from-file"))) {
        baseIcon = QIcon(iconNames.takeFirst());
    } else {
        baseIcon = QIcon::fromTheme(iconNames.takeFirst());
    }
    if (baseIcon.isNull()) {
        qCritical() << "Invalid base icon";
        exit(2);
    }

    QList<QPair<QString, QColor>> overlays;
    OverlayIconEngine *engine;
    if (!parser.isSet(QStringLiteral("convenience-function"))) {
        engine = new OverlayIconEngine(baseIcon);
    }

    if ((iconNames.count() % 2) != 0) {
        qCritical() << "Provide name and color for every overlay.";
        exit(3);
    }

    for (int i = 0; i < iconNames.count(); i += 2) {
        QIcon overlayIcon;
        bool intentionallyInvalid;
        if (iconNames.at(i) == "-") {
            intentionallyInvalid = true;
        } else if (parser.isSet(QStringLiteral("from-file"))) {
            overlayIcon = QIcon(iconNames.at(i));
        } else {
            overlayIcon = QIcon::fromTheme(iconNames.at(i));
        }
        if (overlayIcon.isNull() && !intentionallyInvalid) {
            qCritical() << "Invalid overlay icon:" << iconNames.at(i);
            exit(4);
        }

        QColor overlayColor(iconNames.at(i + 1));
        if (!overlayColor.isValid()) {
            qCritical() << "Invalid overlay color:" << iconNames.at(i + 1);
            exit(5);
        }

        if (parser.isSet(QStringLiteral("convenience-function"))) {
            overlays.append(QPair<QString, QColor>(iconNames.at(i), overlayColor));
        } else {
            engine->addOverlay(overlayIcon, overlayColor);
        }
    }

    if (parser.isSet(QStringLiteral("convenience-function"))) {
        return OverlayIconEngine::fromTheme(baseIcon.name(), overlays);
    } else {
        return QIcon(engine);
    }
}

void createIconDemoWidget(QWidget &demoWidget, const QIcon &icon)
{
    QVBoxLayout *layout = new QVBoxLayout(&demoWidget);
    demoWidget.setLayout(layout);

    QToolButton *button1 = new QToolButton(&demoWidget);
    button1->setIconSize(QSize(16, 16));
    button1->setAutoRaise(true);
    button1->setPopupMode(QToolButton::MenuButtonPopup);
    QAction *action1 = new QAction(icon, QApplication::translate("main", "Button 1"), button1);
    button1->setDefaultAction(action1);
    QAction *action11 = new QAction(icon, QApplication::translate("main", "Action 1"), button1);
    button1->addAction(action11);
    QAction *action12 = new QAction(icon, QApplication::translate("main", "Action 2"), button1);
    button1->addAction(action12);
    layout->addWidget(button1);

    QToolButton *button2 = new QToolButton(&demoWidget);
    button2->setIconSize(QSize(22, 22));
    button2->setPopupMode(QToolButton::MenuButtonPopup);
    QAction *action2 = new QAction(icon, QApplication::translate("main", "Button 2"), button2);
    button2->setDefaultAction(action2);
    QAction *action21 = new QAction(icon, QApplication::translate("main", "Action 1"), button2);
    action21->setCheckable(true);
    button2->addAction(action21);
    QAction *action22 = new QAction(icon, QApplication::translate("main", "Action 2"), button2);
    action22->setCheckable(true);
    button2->addAction(action22);
    layout->addWidget(button2);

    QToolButton *button3 = new QToolButton(&demoWidget);
    button3->setIconSize(QSize(32, 32));
    button3->setPopupMode(QToolButton::MenuButtonPopup);
    QAction *action3 = new QAction(icon, QApplication::translate("main", "Button 3"), button3);
    action3->setCheckable(true);
    button3->setDefaultAction(action3);
    QActionGroup *actionGroup3 = new QActionGroup(button3);
    QAction *action31 = new QAction(icon, QApplication::translate("main", "Action 1"), button3);
    action31->setCheckable(true);
    actionGroup3->addAction(action31);
    button3->addAction(action31);
    QAction *action32 = new QAction(icon, QApplication::translate("main", "Action 2"), button3);
    action32->setCheckable(true);
    actionGroup3->addAction(action32);
    button3->addAction(action32);
    layout->addWidget(button3);

    QToolButton *button4 = new QToolButton(&demoWidget);
    button4->setIconSize(QSize(32, 25));
    button4->setPopupMode(QToolButton::MenuButtonPopup);
    QAction *action4 = new QAction(icon, QApplication::translate("main", "Button 4"), button4);
    button4->setDefaultAction(action4);
    QAction *action41 = new QAction(icon, QApplication::translate("main", "Enable button 3"), button4);
    action41->setCheckable(true);
    action41->setChecked(true);
    button4->addAction(action41);
    QAction *action42 = new QAction(icon, QApplication::translate("main", "Enable action in button 3’s menu"), button4);
    action42->setCheckable(true);
    action42->setChecked(true);
    button4->addAction(action42);
    QAction *action43 = new QAction(icon, QApplication::translate("main", "Action 3"), button4);
    action43->setCheckable(true);
    button4->addAction(action43);
    layout->addWidget(button4);

    QObject::connect(action41, &QAction::toggled, action3, &QAction::setEnabled);
    QObject::connect(action42, &QAction::toggled, action31, &QAction::setEnabled);

    QPixmap zoomPixmap1 = icon.pixmap(QSize(16, 16));
    zoomPixmap1 = zoomPixmap1.scaled(zoomPixmap1.size() * 5);
    zoomPixmap1.setDevicePixelRatio(1.0);
    QLabel *zoomLabel1 = new QLabel(&demoWidget);
    zoomLabel1->setPixmap(zoomPixmap1);
    layout->addWidget(zoomLabel1);

    QPixmap zoomPixmap2 = icon.pixmap(QSize(22, 22));
    zoomPixmap2 = zoomPixmap2.scaled(zoomPixmap2.size() * 5);
    zoomPixmap2.setDevicePixelRatio(1.0);
    QLabel *zoomLabel2 = new QLabel(&demoWidget);
    zoomLabel2->setPixmap(zoomPixmap2);
    layout->addWidget(zoomLabel2);

    QPixmap zoomPixmap3 = icon.pixmap(QSize(22, 22) / icon.pixmap(QSize(22, 22)).devicePixelRatio());
    zoomPixmap3 = zoomPixmap3.scaled(zoomPixmap3.size() * 5);
    zoomPixmap3.setDevicePixelRatio(1.0);
    QLabel *zoomLabel3 = new QLabel(&demoWidget);
    zoomLabel3->setPixmap(zoomPixmap3);
    layout->addWidget(zoomLabel3);
}


// NOTE: Qt::AA_UseHighDpiPixmaps is responsible for upscaling pixmaps.
